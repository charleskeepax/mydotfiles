" An example for a vimrc file.
"
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last change:	2008 Dec 17
"
" To use it, copy it to
"     for Unix and OS/2:  ~/.vimrc
"	      for Amiga:  s:.vimrc
"  for MS-DOS and Win32:  $VIM\_vimrc
"	    for OpenVMS:  sys$login:.vimrc

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching

" Don't use Ex mode, use Q for formatting
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
  set mouse=a
endif

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  " Also don't do it when the mark is in the first line, that is the default
  " position when opening a file.
  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END

else

  set autoindent		" always set autoindenting on

endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

set diffexpr=MyDiff()
function MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  let eq = ''
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      let cmd = '""' . $VIMRUNTIME . '\diff"'
      let eq = '"'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
  else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
endfunction

" Include windows stuff
source $VIMRUNTIME/mswin.vim
behave mswin

" Set font, screen size, and remove silly menus etc from top
if has("gui_running")
  if has("gui_win32")
    set guifont=Consolas:h11:cANSI
  else
    "set guifont=ProggySquareSZ
    "set guifont=Anonymous\ Pro
    set guifont=Inconsolata\ 12
  endif

  set lines=40
  set columns=85
  set guioptions-=m
  set guioptions-=T
endif
set number

" Engage pretty color scheme
set background=dark
colo woria256

" Sort tabs
set tabstop=8
set shiftwidth=8
set list
set listchars=tab:��,trail:�

" Sort wrapping
set nowrap
set guioptions+=b
set listchars+=extends:�

" Turn off the backup files
set nobackup

" Turn on the fold column
set foldcolumn=1

" Set tags file to look in cur dir, then parent and so on
set tags=./tags;

" Add some window shortcuts
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
nmap + <C-w>+

" Move vertically by screen lines rather than file lines
nnoremap j gj
nnoremap k gk

" Function to correct the checksum on intel hex files
function IHexChecksum()
  let l:data = getline(".")
  let l:dlen = strlen(data)

  if (empty(matchstr(l:data, "^:\\(\\x\\x\\)\\{5,}$")))
    echoerr("Input is not a valid Intel HEX line!")
    return
  endif

  let l:byte = 0
  for l:bytepos in range(1, l:dlen-4, 2)
    let l:byte += str2nr(strpart(l:data, l:bytepos, 2), 16)
  endfor

  let l:byte = (256-(l:byte%256))%256
  call setline(".", strpart(l:data, 0, l:dlen-2).printf("%02x", l:byte))
endfunction

" Loads man page for the word under the cursor
function ReadMan()
  " Assign current word under cursor to a script variable:
  let l:man_word = expand('<cword>')
  " Open a new window:
  :wincmd n
  " Read in the manpage for man_word (col -b is for formatting):
  :exe ":r!man " . l:man_word . " | col -b"
  :goto
  :delete
  :set filetype=man
  :resize 15
endfunction

command! -nargs=? -range Dec2hex call s:Dec2hex(<line1>, <line2>, '<args>')
function! s:Dec2hex(line1, line2, arg) range
	if empty(a:arg)
		if histget(':', -1) =~# "^'<,'>" && visualmode() !=# 'V'
			let cmd = 's/\%V\<\d\+\>/\=printf("0x%x",submatch(0)+0)/g'
		else
			let cmd = 's/\<\d\+\>/\=printf("0x%x",submatch(0)+0)/g'
		endif
		try
			execute a:line1 . ',' . a:line2 . cmd
		catch
			echo 'Error: No decimal number found'
		endtry
	else
		echo printf('%x', a:arg + 0)
	endif
endfunction

command! -nargs=? -range Hex2dec call s:Hex2dec(<line1>, <line2>, '<args>')
function! s:Hex2dec(line1, line2, arg) range
	if empty(a:arg)
		if histget(':', -1) =~# "^'<,'>" && visualmode() !=# 'V'
			let cmd = 's/\%V0x\x\+/\=submatch(0)+0/g'
		else
			let cmd = 's/0x\x\+/\=submatch(0)+0/g'
		endif
		try
			execute a:line1 . ',' . a:line2 . cmd
		catch
			echo 'Error: No hex number starting "0x" found'
		endtry
	else
		echo (a:arg =~? '^0x') ? a:arg + 0 : ('0x'.a:arg) + 0
	endif
endfunction

" Some terminals need this for 256 color support
set t_Co=256

abbr lkmlack Acked-by: Charles Keepax <ckeepax@opensource.cirrus.com>
abbr lkmlreview Reviewed-by: Charles Keepax <ckeepax@opensource.cirrus.com>
abbr lkmltest Tested-by: Charles Keepax <ckeepax@opensource.cirrus.com>

abbr GitSign Signed-off-by: Charles Keepax <ckeepax@opensource.cirrus.com>
command ChID read !ChangeId.sh

abbr KDbg pr_err("CK -- In %s\n", __func__);

set wildmode=longest,list,full
set wildmenu
