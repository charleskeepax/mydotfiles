if exists("b:current_syntax")
  finish
endif

syn case ignore

syn region noteMainTitle start="\*\*" end="\*\*" contains=noteTitleDate
syn match noteTitleDate "..../../.." contained
hi def link noteMainTitle Type
hi def link noteTitleDate Keyword

syn match noteSubTitle "^--.*--$"
hi def link noteSubTitle Include

"syn region noteCode start=":\n  " end="\n\n"
syn match noteCode "^  .*$"
hi def link noteCode String

syn match noteURL "^https\=://.*\..*/\=.*$"
hi def link noteURL Special
