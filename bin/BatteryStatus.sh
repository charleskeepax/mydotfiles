BATDEV=/sys/class/power_supply/$1

if [ ! -f $BATDEV/status ]; then
    exit
fi

STATUS=$(cat $BATDEV/status)
BATFULL=$(cat $BATDEV/energy_full)
BATNOW=$(cat $BATDEV/energy_now)

PERCENT=$(( 100 * $BATNOW / $BATFULL ))

if [ $PERCENT -lt 30 ]; then
    PERCENT="$PERCENT% LOW!"
else
    PERCENT="$PERCENT%"
fi

echo "$STATUS $PERCENT"
