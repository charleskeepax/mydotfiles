#!/bin/bash

echo -n "Enter the Artist: "
read artist
echo -n "Enter the Album: "
read album
echo -n "Enter the Year: "
read year
echo -n "Enter the Genre: "
read genre

for f in *.wav
do
  echo "========================================="
  echo "Processing $f ..."
  echo -n "Enter the Track Name: "
  read track
  tracknum=$(echo $f | sed 's/.*track\(.*\)\.cdda\.wav/\1/')
  trimtracknum=$(echo $tracknum | sed 's/0*\(.*\)/\1/')
  newname="$artist - $album - $tracknum.$track.flac"
  echo "Creating $newname ..."
  flac --best -fe "$f" -o "$newname"
  metaflac --set-tag="TITLE=$track" --set-tag="TRACKNUMBER=$trimtracknum" --set-tag="ARTIST=$artist" --set-tag="ALBUM=$album" --set-tag="DATE=$year" --set-tag="GENRE=$genre" "$newname"
  echo "DONE!"
done

echo -n "Delete original WAV Files (y/n)? "
read delstuff
if [ "$delstuff" == "y" ] ; then
  rm -f *.wav
fi

