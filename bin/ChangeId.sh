_gen_ChangeIdInput() {
	echo "tree `git write-tree`"
	if parent=`git rev-parse HEAD^0 2>/dev/null`
	then
		echo "parent $parent"
	fi
	echo "author `git var GIT_AUTHOR_IDENT`"
	echo "committer `git var GIT_COMMITTER_IDENT`"
	echo
	printf '%s' "$clean_message"
}

_gen_ChangeId() {
	_gen_ChangeIdInput |
	git hash-object -t commit --stdin
}

echo "Change-Id: I$(_gen_ChangeId)"
